# dkzr/satis

A [composer/satis](https://github.com/composer/satis) implementation with push hook.

## Install

1. Clone repository to desired destination.
2. Run `composer install` to retrieve dependencies (mainly: the dev-master of composer/satis).
3. Copy or rename `private/config-example.php` to `private/config.php` and add a `hook_secret` value.
4. Copy or rename `private/satis-example.json` to `private/satis.json` and change `homepage` value to where you want your satis to be available online.
5. In `private/satis.json`, remove the demo-content in the `repositories`  section and add your own repositories there.
6. Run `bin/dkzr-satis` to fetch your repository data and output the first packages data to `public_html/`
7. Configure your webserver to serve the `public_html/` directory when you request the url you configured as `homepage` in your `private/satis.json` 
8. Optionally, configure cron to run `bin/dkzr-satis -q` at regular intervals to update all repository data.

More information about the available options in satis.json can be found in the [satis docs](https://getcomposer.org/doc/articles/handling-private-packages-with-satis.md#satis).

## Using the Push hook

The dkzr/satis webhook can be called by a GET or POST to [https://composer.example.com/hook/](https://composer.example.com/hook/).

### GitLab webhook

1. Go to your repository on GitLab, then open Settings - Webhooks.
2. URL field: use your hook url, eg. https://composer.example.com/hook/
3. Secret Token field: use the `hook_secret` as defined in `private/config.php`.
4. Select the triggers "Push events" and "Tag push events".

### GitHub webhook

1. Go to your repository on GitHub, then open Settings - Webhooks.
2. Click "Add webhook" (Confirm password to continue).
3. Payload URL field: use your hook url, eg. https://composer.example.com/hook/
4. Content type field: select "application/x-www-form-urlencoded".
5. Secret field: use the `hook_secret` as defined in `private/config.php`.
6. Select "Just the push event" for triggers.

### Bitbucket webhook

1. Go to your repository on Bitbucket, then open Repository settings - Webhooks.
2. Click "Add webhook".
3. Title field: give your hook a title.
4. URL field: use your hook url with a `secret` parameter. The value of the secret parameter is the `hook_secret` defined in `private/config.php`.  
   Example: https://composer.example.com/hook/?secret=defined-in-private-config
5. Select "Repository push" for triggers.

### HTTP GET request to hook

For testing or other systems currently not supported you can call the webhook using HTTP GET parameters. Required querystring parameters are:

- (string) **secret** - `hook_secret` as defined in `private/config.php`
- (string) **repository** - URL of one of the repositories as defined in `private/satis.json`

Example: https://composer.example.com/hook/?secret=defined-in-private-config&repository=https://git.example.com/private.git
