<?php
header( 'Content-Type: text/plain' );

define( 'PRIVATE_DIR', dirname( dirname( __DIR__ ) ) . '/private' );
require_once PRIVATE_DIR . '/vendor/autoload.php';

$config = require PRIVATE_DIR . '/config.php';

if ( ! isset( $config['hook_secret'], $config['satis_command']['file'], $config['satis_command']['output-dir'] ) ) {
  header( 'HTTP/1.0 500 Internal server error', true, 500 );
  exit( 'Please check your config.php' );
}
if ( ! isset( $config['debug'] ) ) {
  $config['debug'] = false;
}
if ( ! isset( $config['satis_command']['command'] ) ) {
  $config['satis_command']['command'] = 'build';
}

if ( $config['debug'] ) {
  ini_set( 'log_errors', 1 );
  ini_set( 'error_log', PRIVATE_DIR . '/debug.log' );

  //error_log( print_r( [
  //  'server' => $_SERVER,
  //  'post' => $_POST,
  //  'get' => $_GET,
  //  'input' => file_get_contents( 'php://input' ),
  //], 1 ) );
}

if ( isset( $_SERVER['HTTP_X_HUB_SIGNATURE'] ) || isset( $_SERVER['HTTP_X_HUB_SIGNATURE_256'] ) ) {
  // WebSub request
  if ( isset( $_SERVER['HTTP_X_HUB_SIGNATURE_256'] ) ) {
    $signature = $_SERVER['HTTP_X_HUB_SIGNATURE_256'];
  } else {
    $signature = $_SERVER['HTTP_X_HUB_SIGNATURE'];
  }

  $algo = substr( $signature, 0, strpos( $signature, '=' ) );
  if ( ! in_array( $algo, hash_hmac_algos() ) ) {
    header( 'HTTP/1.0 400 Bad request', true, 400 );
    exit( 'Not a valid WebSub request: unsupported algorithm' );
  }
  $hash = $algo . '=' . hash_hmac( $algo, file_get_contents( 'php://input' ), $config['hook_secret'] );
  if ( 0 == strcmp( $signature, $hash ) ) {
    if ( isset( $_POST['payload'] ) ) {
      // GitHub variant
      $json = json_decode( $_POST['payload'] );
    } else {
      $json = json_decode( file_get_contents( 'php://input' ) );
    }
    if ( is_object( $json ) ) {
      if ( str_starts_with( $_SERVER['HTTP_USER_AGENT'], 'GitHub' ) && isset( $json->repository->clone_url ) ) {
        // GitHub
        $config['satis_command']['--repository-url'] = $json->repository->clone_url;
      } else if ( str_starts_with( $_SERVER['HTTP_USER_AGENT'], 'Bitbucket' ) && isset( $json->repository->scm, $json->repository->links->html->href ) ) {
        // Bitbucket
        $config['satis_command']['--repository-url'] = $json->repository->links->html->href . '.' . $json->repository->scm;
      }
    }
  }
  if( empty( $config['satis_command']['--repository-url'] ) ) {
    header( 'HTTP/1.0 400 Bad request', true, 400 );
    exit( 'Not a valid WebSub request' );
  }
} else if ( str_starts_with( $_SERVER['HTTP_USER_AGENT'], 'GitLab' ) && isset( $_SERVER['HTTP_X_GITLAB_TOKEN'] ) && $config['hook_secret'] == (string) $_SERVER['HTTP_X_GITLAB_TOKEN'] ) {
  // GitLab request
  $post = file_get_contents( 'php://input' );
  $json = json_decode( $post );
  if ( is_object( $json ) && ( 'push' == $json->object_kind || 'tag_push' == $json->object_kind ) ) {
    $config['satis_command']['--repository-url'] = $json->repository->git_http_url;
  } else {
    header( 'HTTP/1.0 400 Bad request', true, 400 );
    exit( 'Not a valid GitLab request' );
  }
} else if ( str_starts_with( $_SERVER['HTTP_USER_AGENT'], 'Bitbucket' ) && isset( $_SERVER['HTTP_X_EVENT_KEY'], $_GET['secret'] ) && $config['hook_secret'] == (string) $_GET['secret'] && 'repo:push' == $_SERVER['HTTP_X_EVENT_KEY'] ) {
  // Unsecured Bitbucket request
  $post = file_get_contents( 'php://input' );
  $json = json_decode( $post );
  if ( is_object( $json ) && isset( $json->repository->scm, $json->repository->links->html->href ) ) {
    $config['satis_command']['--repository-url'] = $json->repository->links->html->href . '.' . $json->repository->scm;
  } else {
    header( 'HTTP/1.0 400 Bad request', true, 400 );
    exit( 'Not a valid Bitbucket request' );
  }
} else if ( isset( $_GET['repository'], $_GET['secret'] ) && $config['hook_secret'] == (string) $_GET['secret'] ) {
  // HTTP GET request
  $config['satis_command']['--repository-url'] = (string) $_GET['repository'];
}

if ( $config['debug'] ) {
  //$post = file_get_contents( 'php://input' );
  //$json = json_decode( $post );
  //error_log( print_r( $json, 1 ) );
  //error_log( print_r( [ $_SERVER, $_REQUEST ], 1 ) );
  error_log( print_r( $config['satis_command'], 1 ) );
}

if ( isset( $config['satis_command']['--repository-url'] ) ) {
  // --repository-url must be array
  $config['satis_command']['--repository-url'] = [ $config['satis_command']['--repository-url'] ];

  $application = new Composer\Satis\Console\Application();
  $application->setAutoExit( false );
  
  $input = new Symfony\Component\Console\Input\ArrayInput( $config['satis_command'] );
  $output = new Symfony\Component\Console\Output\BufferedOutput();
  
  $application->run( $input, $output );

  if ( $config['debug'] ) {
    error_log( print_r( $output->fetch(), 1 ) );
  }
  exit( 'Valid request' );
} else {
  header( 'HTTP/1.0 400 Bad request', true, 400 );
  exit( 'Not a valid request' );
}
